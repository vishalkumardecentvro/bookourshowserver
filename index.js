const express = require("express");
const mysql = require("mysql");
const app = express();
const bodyParser = require("body-parser");
var cors = require("cors");

app.use(
  bodyParser.urlencoded({
    // to support URL-encoded bodies
    extended: true,
  })
);

app.use(bodyParser.json());

app.use(cors());
// create connection
const db = mysql.createConnection({
  host: "localhost",
  user: "movie",
  password: "Vishal@123456",
  database: "BOOK_OUR_SHOW",
});

// connect

db.connect((err) => {
  if (err) {
    throw err;
  } else console.log("mysql connected");
});

// routes
app.get("/createDb", (req, res) => {
  let sql =
    "CREATE TABLE Movies( id int NOT NULL auto_increment, Movie_name varchar(255) NOT NULL, PRIMARY KEY (id))";
  db.query(sql, (err, result) => {
    if (err) throw err;
    else res.send("table created");

    console.log(result);
  });
});

app.get("/check", (req, res) => {
  res.send("All fine");
});

app.post("/sign_in", (req, res) => {
  console.log("body", req.body.First_name);

  let userVerification = `SELECT * FROM User WHERE User_email='${req.body.User_email}'`;
  let verficationQery = db.query(userVerification, (err, result) => {
    if (err) {
      throw err;
    } else {
      res.sendStatus(401);
    }
  });

  let Sign_in_detail = {
    First_name: `${req.body.firstName}`,
    Last_name: `${req.body.lastName}`,
    User_email: `${req.body.email}`,
    User_password: `${req.body.password}`,
    User_state: `${req.body.state}`,
  };

  let sql = "INSERT INTO `User` SET ?";
  let query = db.query(sql, Sign_in_detail, (err, result) => {
    if (err) {
      res.sendStatus(400);
      throw err;
    } else {
      res.sendStatus(200);
      console.log("body", Sign_in_detail);
    }
  });
});

app.get("/log_in/:email/:password/:userState", (req, res) => {
  console.log("inside", req.params);
  let sql = `SELECT admin_email, admin_password FROM admin WHERE admin_email = '${req.params.email}' && admin_password = '${req.params.password}'`;
  let query = db.query(sql, (err, result) => {
    if (err) {
      throw err;
    } else {
      if (result.length === 0) {
        let sql = `SELECT User_email, User_password, User_state FROM User WHERE User_email = '${req.params.email}' && User_state = '${req.params.userState}' && User_password = '${req.params.password}'`;
        let query = db.query(sql, (err, result) => {
          if (err) {
            throw err;
          } else {
            if (result.length == null) {
              res.send(404);
              return;
            }
            let userStateSql = `UPDATE User SET User_state= '${req.params.userState}' WHERE User_email = '${req.params.email}'`;

            let user_state_query = db.query(userStateSql, (err, result) => {
              if (err) {
                throw err;
              } else {
                console.log(userStateSql);
                res.send({ status: 200, isAdmin: false });
                return;
              }
            });
          }
        });
      } else {
        console.log(result);
        res.send({ status: 200, isAdmin: true });
        return;
      }
    }
  });
});

app.get("/activeStatus/:activeMail", (req, res) => {
  console.log("inside user verification");
  let sql = `SELECT * FROM User WHERE User_email = '${req.params.activeMail}' && User_state = 'active'`;
  console.log(`${req.params.activeMail}`);
  let query = db.query(sql, (err, result) => {
    if (err) {
      throw err;
    } else if (result.length > 0) {
      console.log("statusResult", result);
      res.sendStatus(200);
    } else {
      res.sendStatus(400);
    }
  });
});

app.get("/cities", (req, res) => {
  let sql = "SELECT * FROM CITY";
  let query = db.query(sql, (err, result) => {
    if (err) {
      throw err;
    } else {
      console.log("result", result);
      res.send(result);
    }
  });
});

app.get("/log_out_user/:email", (req, res) => {
  console.log(req.params.email);
  let sql = `UPDATE User SET User_state = 'registered' WHERE User_email = '${req.params.email}'`;
  let query = db.query(sql, (err, result) => {
    if (err) {
      throw err;
    } else {
      console.log(result);
      res.sendStatus(200);
    }
  });
});

app.get("/theatre", (req, res) => {
  let cityId = `SELECT city_id FROM CITY WHERE city_name='Kolkata'`;

  let query = db.query(cityId, async (err, result) => {
    if (err) {
      throw err;
    } else {
      console.log("my id", result[0].city_id);
      let theatrSql = `SELECT * FROM Theatre WHERE city_id = '${result[0].city_id}'`;

      let theatreQuery = db.query(theatrSql, (err, theatreResult) => {
        if (err) {
          throw err;
        } else {
          console.log(theatreResult);
          res.send(theatreResult);
        }
      });
    }
  });
});

app.get("/movies", (req, res) => {
  let sql = `SELECT * FROM Movies `;
  let query = db.query(sql, (err, result) => {
    if (err) {
      throw err;
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

app.get("/allTheatres", (req, res) => {
  let sql = `SELECT * FROM Theatre `;
  let query = db.query(sql, (err, result) => {
    if (err) {
      throw err;
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

app.post("/theatres", (req, res) => {
  let sql = `SELECT * FROM Theatre T,CITY C,Movies M WHERE T.city_id = C.city_id AND M.movie_id = T.movie_id AND city_name = '${req.body.city_name}' AND movie_name = '${req.body.Movie_name}' AND show_date = '${req.body.show_date}'`;
  console.log("theatres--", req.body);
  let query = db.query(sql, (err, result) => {
    if (err) {
      throw err;
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

app.get("/movie_summary/:id", (req, res) => {
  let sql = `select movie_summary from Movies where movie_id = ${req.params.id}`;
  let query = db.query(sql, (err, result) => {
    if (err) {
      throw err;
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

app.get("/moviePrice/:id", (req, res) => {
  let sql = `select price from Movies where movie_id = ${req.params.id}`;
  let query = db.query(sql, (err, result) => {
    if (err) {
      throw err;
    } else {
      res.send(result);
    }
  });
});

app.post("/payments", (req, res) => {
  let user_id = 0;

  let sql = `select id from User where user_email = '${req.body.mail}'`;
  let query = db.query(sql, (err, result) => {
    if (err) {
      throw err;
    } else {
      console.log(result);
      id = result[0].id;

      let pay_Object = {
        reference_number: `${req.body.reference_number}`,
        payment_status: `${req.body.status}`,
        user_id: result[0].id,
        payment_date: `${req.body.payment_date}`,
        payee_mail: `${req.body.mail}`,
        payee_contact: `${req.body.payee_contact}`,
      };

      let pay_query = "insert into `payments` set ?";
      let save_pay_db = db.query(pay_query, pay_Object, (err, result) => {
        if (err) {
          throw err;
        } else {
          console.log("pay obj", pay_Object);
          console.log("pay result", result);

          //updating seats

          let query = `update Theatre set vacant_seats = vacant_seats - ${req.body.seats} where theatre_id = ${req.body.theatreId}`;
          console.log("------updating seats");
          let sql = db.query(query, (err, result) => {
            if (err) {
              throw err;
            } else {
              console.log("seats updated in theatre");
              let ticketObj = {
                movie_name: req.body.movieName,
                no_of_seats: req.body.seats,
                time: req.body.showTime,
                date: req.body.showDate,
              };
              let ticketSaveQuery = `insert into tickets set?`;
              let ticketQuery = db.query(
                ticketSaveQuery,
                ticketObj,
                (err, result) => {
                  if (err) {
                    throw err;
                  } else {
                    res.sendStatus(200);
                  }
                }
              );
            }
          });
        }
      });
    }
  });
});

// app.get("/adminLog_in/:email/:password", (req, res) => {
//   let sql = `SELECT admin_email, admin_password FROM admin WHERE admin_email = '${req.params.email}' && admin_password = '${req.params.password}'`;
//   let query = db.query(sql, (err, result) => {
//     if (err) {
//       throw err;
//     } else {
//       if (result.length === 0) {
//         res.sendStatus(404);
//         return;
//       } else {
//         console.log(result);
//         res.sendStatus(200);
//       }
//     }
//   });
// });

app.post("/addMovies", (req, res) => {
  console.log("---------------------", req.body);
  const body = req.body;
  let movie = {
    Movie_name: body.movie_name,
    Movie_rating: body.movie_rating,
    Movie_language: body.movie_language,
    Movie_poster_link: body.movie_poster_link,
    movie_summary: body.movie_summary,
    price: body.price,
    Movie_type: body.movie_type,
  };
  console.log("movie body", body);
  console.log("object ", movie);

  let sql = `insert into Movies set ?`;
  let query = db.query(sql, movie, (err, result) => {
    if (err) {
      throw err;
    } else {
      console.log("-------------res", result);
      res.sendStatus(200);
    }
  });
});

app.get("/adminMovieSelectionForTheatres", (req, res) => {
  let sql = `select Movie_name from Movies`;
  let query = db.query(sql, (err, result) => {
    if (err) {
      throw err;
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

app.get("/adminCitySelectionForTheatres", (req, res) => {
  let sql = `select city_name from CITY`;
  let query = db.query(sql, (err, result) => {
    if (err) {
      throw err;
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

app.post("/saveTheatre", (req, res) => {
  // for saving theatre
  console.log("api hit");
  console.log("body", req.body);
  let sql = `select Movie_id from Movies where Movie_name = '${req.body.theatre_movie}' union all select city_id from CITY where city_name = '${req.body.theatre_movie_city}' `;
  let query = db.query(sql, (err, result) => {
    try {
      console.log("theatre query", result[0].Movie_id, result[1].Movie_id); // first one is movie id and second one is city id
      let theatreObject = {
        theatre_name: req.body.theatre_name,
        total_seats: req.body.total_seats,
        vacant_seats: req.body.total_seats, // total seats and vacant seats will be same initially
        Movie_id: result[0].Movie_id,
        city_id: result[1].Movie_id,
        show_time: req.body.show_time,
        show_date: req.body.show_date,
        theatre_area: req.body.theatre_location,
      };

      let theatreSql = "insert into Theatre set?";
      let theatreSavingQuery = db.query(
        theatreSql,
        theatreObject,
        (err, result) => {
          if (err) {
            throw err;
          } else {
            console.log(theatreObject);
            console.log("theatre insert", result);
            res.sendStatus(200);
          }
        }
      );
    } catch {
      throw err;
    }
  });
});

app.delete("/deleteTheatre/:theatre_name", (req, res) => {
  console.log("--------del", req.params);
  let sql = `delete from Theatre where theatre_name = '${req.params.theatre_name}'`;
  let query = db.query(sql, (err, result) => {
    if (err) {
      throw err;
    } else {
      //console.log(result)
      console.log("deleted theatre - ", req.params.theatre_name);
      res.sendStatus(200);
    }
  });
});

app.delete("/deletemovie/:id", (req, res) => {
  console.log("del", req.params);
  let sql = `update Theatre set Movie_id = null where Movie_id = '${req.params.id}'`; // first update theatres which show this movie
  let query = db.query(sql, (err, result) => {
    if (err) {
      throw err;
    } else {
      let deleteSql = ` delete from Movies where Movie_id = ${req.params.id} `; // then delete that movie from movie table
      let deleteQuery = db.query(deleteSql, (err, result) => {
        if (err) {
          throw err;
        } else {
          console.log(result);
          console.log("deleted movie - ", req.params.id);
          res.sendStatus(200);
        }
      });
    }
  });
});

app.post("/updatetheatreseats", (req, res) => {
  let query = `update Theatre set vacant_seats = vacant_seats - ${req.body.seats} where theatre_id = ${req.body.id}`;
  let sql = db.query(query, (err, result) => {
    if (err) {
      throw err;
    } else {
      console.log("seats updated in theatre");
      res.sendStatus(200);
    }
  });
});

app.get("/singlemovie/:id", (req, res) => {
  let sql = `select * from Movies where Movie_id = ${req.params.id}`;
  let query = db.query(sql, (err, result) => {
    if (err) {
      throw err;
    } else {
      res.send(result);
    }
  });
});

app.listen("3000", () => {
  console.log("server started at port 3000");
});
